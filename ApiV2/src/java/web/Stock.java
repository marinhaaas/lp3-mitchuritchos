/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rúben Marinheiro
 */
@Entity
@Table(name = "Stock")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stock.findAll", query = "SELECT s FROM Stock s")
    , @NamedQuery(name = "Stock.findByStockID", query = "SELECT s FROM Stock s WHERE s.stockID = :stockID")
    , @NamedQuery(name = "Stock.findByOrderLineID", query = "SELECT s FROM Stock s WHERE s.orderLineID = :orderLineID")
    , @NamedQuery(name = "Stock.findByPaperGsm", query = "SELECT s FROM Stock s WHERE s.paperGsm = :paperGsm")
    , @NamedQuery(name = "Stock.findByPaperLength", query = "SELECT s FROM Stock s WHERE s.paperLength = :paperLength")
    , @NamedQuery(name = "Stock.findByPaperWidth", query = "SELECT s FROM Stock s WHERE s.paperWidth = :paperWidth")
    , @NamedQuery(name = "Stock.findByQuantitySheet", query = "SELECT s FROM Stock s WHERE s.quantitySheet = :quantitySheet")})
public class Stock implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "StockID")
    private Integer stockID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OrderLineID")
    private int orderLineID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PaperGsm")
    private int paperGsm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PaperLength")
    private int paperLength;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PaperWidth")
    private int paperWidth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QuantitySheet")
    private int quantitySheet;

    public Stock() {
    }

    public Stock(Integer stockID) {
        this.stockID = stockID;
    }

    public Stock(Integer stockID, int orderLineID, int paperGsm, int paperLength, int paperWidth, int quantitySheet) {
        this.stockID = stockID;
        this.orderLineID = orderLineID;
        this.paperGsm = paperGsm;
        this.paperLength = paperLength;
        this.paperWidth = paperWidth;
        this.quantitySheet = quantitySheet;
    }

    public Integer getStockID() {
        return stockID;
    }

    public void setStockID(Integer stockID) {
        this.stockID = stockID;
    }

    public void setOrderLineID(int orderLineID) {
        this.orderLineID = orderLineID;
    }

    public int getPaperGsm() {
        return paperGsm;
    }

    public void setPaperGsm(int paperGsm) {
        this.paperGsm = paperGsm;
    }

    public int getPaperLength() {
        return paperLength;
    }

    public void setPaperLength(int paperLength) {
        this.paperLength = paperLength;
    }

    public int getPaperWidth() {
        return paperWidth;
    }

    public void setPaperWidth(int paperWidth) {
        this.paperWidth = paperWidth;
    }

    public int getQuantitySheet() {
        return quantitySheet;
    }

    public void setQuantitySheet(int quantitySheet) {
        this.quantitySheet = quantitySheet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (stockID != null ? stockID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stock)) {
            return false;
        }
        Stock other = (Stock) object;
        if ((this.stockID == null && other.stockID != null) || (this.stockID != null && !this.stockID.equals(other.stockID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "web.Stock[ stockID=" + stockID + " ]";
    }
    
}
