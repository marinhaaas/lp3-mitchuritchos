/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rúben Marinheiro
 */
@Entity
@Table(name = "Encomenda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encomenda.findAll", query = "SELECT e FROM Encomenda e")
    , @NamedQuery(name = "Encomenda.findByIDEncomenda", query = "SELECT e FROM Encomenda e WHERE e.iDEncomenda = :iDEncomenda")
    , @NamedQuery(name = "Encomenda.findByDate", query = "SELECT e FROM Encomenda e WHERE e.date = :date")
    , @NamedQuery(name = "Encomenda.findByAddress", query = "SELECT e FROM Encomenda e WHERE e.address = :address")
    , @NamedQuery(name = "Encomenda.findByCodPostal", query = "SELECT e FROM Encomenda e WHERE e.codPostal = :codPostal")
    , @NamedQuery(name = "Encomenda.findByCountry", query = "SELECT e FROM Encomenda e WHERE e.country = :country")
    , @NamedQuery(name = "Encomenda.findByMoeda", query = "SELECT e FROM Encomenda e WHERE e.moeda = :moeda")
    , @NamedQuery(name = "Encomenda.findByTotal", query = "SELECT e FROM Encomenda e WHERE e.total = :total")})
public class Encomenda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = true)
    @Size(min = 1, max = 36)
    @Column(name = "IDEncomenda")
    private String iDEncomenda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CodPostal")
    private String codPostal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "Moeda")
    private String moeda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Total")
    private long total;
    @JoinColumn(name = "iDEncomenda", referencedColumnName = "iDEncomenda")
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iDEncomenda")
    private Collection<LinhaEncomenda> linhaEncomendaCollection;
    @JoinColumn(name = "IDCliente", referencedColumnName = "IDCliente")
    @ManyToOne(optional = false)
    private Cliente iDCliente;

    public Encomenda() {
    }

    public Encomenda(String iDEncomenda) {
        this.iDEncomenda = iDEncomenda;
    }

    public Encomenda(String iDEncomenda, Date date, String address, String codPostal, String country, String moeda, long total) {
        this.iDEncomenda = iDEncomenda;
        this.date = date;
        this.address = address;
        this.codPostal = codPostal;
        this.country = country;
        this.moeda = moeda;
        this.total = total;
    }

    public String getIDEncomenda() {
        return iDEncomenda;
    }

    public void setIDEncomenda(String iDEncomenda) {
        this.iDEncomenda = iDEncomenda;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    @XmlTransient
    public Collection<LinhaEncomenda> getLinhaEncomendaCollection() {
        return linhaEncomendaCollection;
    }

    public void setLinhaEncomendaCollection(Collection<LinhaEncomenda> linhaEncomendaCollection) {
        this.linhaEncomendaCollection = linhaEncomendaCollection;
    }

    public Cliente getIDCliente() {
        return iDCliente;
    }

    public void setIDCliente(Cliente iDCliente) {
        this.iDCliente = iDCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDEncomenda != null ? iDEncomenda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encomenda)) {
            return false;
        }
        Encomenda other = (Encomenda) object;
        if ((this.iDEncomenda == null && other.iDEncomenda != null) || (this.iDEncomenda != null && !this.iDEncomenda.equals(other.iDEncomenda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "web.Encomenda[ iDEncomenda=" + iDEncomenda + " ]";
    }
    
}
