/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rúben Marinheiro
 */
@Entity
@Table(name = "LinhaEncomenda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LinhaEncomenda.findAll", query = "SELECT l FROM LinhaEncomenda l")
    , @NamedQuery(name = "LinhaEncomenda.findByIDLinha", query = "SELECT l FROM LinhaEncomenda l WHERE l.iDLinha = :iDLinha")
    , @NamedQuery(name = "LinhaEncomenda.findByNome", query = "SELECT l FROM LinhaEncomenda l WHERE l.nome = :nome")
    , @NamedQuery(name = "LinhaEncomenda.findByQuantity", query = "SELECT l FROM LinhaEncomenda l WHERE l.quantity = :quantity")
    , @NamedQuery(name = "LinhaEncomenda.findByUnit", query = "SELECT l FROM LinhaEncomenda l WHERE l.unit = :unit")
    , @NamedQuery(name = "LinhaEncomenda.findByTax", query = "SELECT l FROM LinhaEncomenda l WHERE l.tax = :tax")
    , @NamedQuery(name = "LinhaEncomenda.findByTotal", query = "SELECT l FROM LinhaEncomenda l WHERE l.total = :total")})
public class LinhaEncomenda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "IDLinha")
    private String iDLinha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Nome")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantity")
    private int quantity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Unit")
    private String unit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tax")
    private long tax;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Total")
    private long total;
    @JoinColumn(name = "IDEncomenda", referencedColumnName = "IDEncomenda")
    @ManyToOne(optional = false)
    private Encomenda iDEncomenda;

    public LinhaEncomenda() {
    }

    public LinhaEncomenda(String iDLinha) {
        this.iDLinha = iDLinha;
    }

    public LinhaEncomenda(String iDLinha, String nome, int quantity, String unit, long tax, long total) {
        this.iDLinha = iDLinha;
        this.nome = nome;
        this.quantity = quantity;
        this.unit = unit;
        this.tax = tax;
        this.total = total;
    }

    public String getIDLinha() {
        return iDLinha;
    }

    public void setIDLinha(String iDLinha) {
        this.iDLinha = iDLinha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public long getTax() {
        return tax;
    }

    public void setTax(long tax) {
        this.tax = tax;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public Encomenda getIDEncomenda() {
        return iDEncomenda;
    }

    public void setIDEncomenda(Encomenda iDEncomenda) {
        this.iDEncomenda = iDEncomenda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDLinha != null ? iDLinha.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LinhaEncomenda)) {
            return false;
        }
        LinhaEncomenda other = (LinhaEncomenda) object;
        if ((this.iDLinha == null && other.iDLinha != null) || (this.iDLinha != null && !this.iDLinha.equals(other.iDLinha))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "web.LinhaEncomenda[ iDLinha=" + iDLinha + " ]";
    }
    
}
