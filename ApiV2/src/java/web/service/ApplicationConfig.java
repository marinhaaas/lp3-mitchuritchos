/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Rúben Marinheiro
 */
@javax.ws.rs.ApplicationPath("feirapaper/resources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(web.service.ClienteFacadeREST.class);
        resources.add(web.service.EncomendaFacadeREST.class);
        resources.add(web.service.LinhaEncomendaFacadeREST.class);
        resources.add(web.service.StockFacadeREST.class);
    }
    
}
