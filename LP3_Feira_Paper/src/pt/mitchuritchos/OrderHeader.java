package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;

public class OrderHeader extends ArrayList<OrderHeader> {
    private String reff;
    private String date;
    private String supplierId;

    public String getReff() {
        return reff;
    }

    public void setReff(String reff) {
        this.reff = reff;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public OrderHeader(String reff, String date, String supplierId) {
        this.reff = reff;
        this.date = date;
        this.supplierId = supplierId;
    }

    /**
     * este método guarda os dados o OrderHeader na Base de Dados
     * @return este método retorna true se for guardado na Base de Dados com sucesso, e false se não guardar
     */
    public boolean save() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {
            String query = "EXEC ImportXMLOrderHeader " +
                    "@OrderReference = '" + this.reff +
                    "', @OrderDate = '" + this.date +
                    "', @SupplierID = " + this.supplierId + ";";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
        return retorno;
    }

}
