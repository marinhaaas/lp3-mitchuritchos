package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Validation {

    /**
     * Este método verifica se id do supplier com logado é igual ao dos dados(id do supllier) do ficheiro
     * @param orderHeader é um Objeto OrderHeader
     * @param supplier é um objeto Supplier
     * @return
     */
    public static boolean isValidSupplierIdXml(OrderHeader orderHeader, Supplier supplier) {
        return orderHeader.getSupplierId().equalsIgnoreCase(supplier.getSupplierID());
    }

    /**
     * Este método verifica se um ficheiro XML inserido é ou não duplicado
     * @param xml Ficheiro XML a verificar
     * @return True caso seja duplicado, False caso não seja
     */
    public static boolean isDuplicatedInput(InputXml xml) {
        boolean retorno = false;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("Select * From OrderHeader Where OrderReference = '" + xml.getOrderHeader().getReff() + "';");

            while (rs.next())
            {
                retorno = true;
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }


    /**
     * Este método verifica se um ficheiro JSON inserido é ou não duplicado
     * @param json Ficheiro XML a verificar
     * @return True caso seja duplicado, False caso não seja
     */
    public static boolean isDuplicatedInput(InputJson json) {
        boolean retorno = false;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("Select * From OrderHeader Where OrderReference = '" + json.getOrderHeader().getReff() + "';");

            while (rs.next())
            {
                retorno = true;
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }

    /**
     * Este método verifica se ja existe na base de dados papel com comprimento igual ao do ficheiro
     * @param stock é um Objeto Stock
     * @return retorna um objeto Stock
     */
    public static Stock checkIfStockExists(Stock stock) {
        Stock retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("Select * From Stock Where PaperLength = '" + stock.getPaperLength() + "' AND PaperWidth = '" + stock.getPaperWidth() + "' AND PaperGsm = '" + stock.getPaperGsm() + "';");

            while (rs.next())
            {
                retorno = new Stock(Integer.parseInt(rs.getString("OrderLineID")), rs.getString("PaperGsm"), rs.getString("PaperLength"), rs.getString("PaperWidth"), rs.getString("QuantitySheet"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }


    public static boolean validarMorada(int IDPerson, String Address1, String Address2, String City, String CodPostal, String CodIso, String Country) {
        Person retorno = null;
        try {

            String query = "Select * from Person WHERE = " + IDPerson;

            Statement st = Connect.connectDb().createStatement();


            ResultSet rs = st.executeQuery(query);


            while (rs.next()) {
                String address1=rs.getString("Address1");
                String address2=rs.getString("Address2");
                String city=rs.getString("City");
                String codpostal=rs.getString("CodPostal");
                String codiso=rs.getString("CodIso");
                String country=rs.getString("Country");

                if(address1.equalsIgnoreCase(Address1) && address2.equalsIgnoreCase(Address2) && city.equalsIgnoreCase(City) && codpostal.equalsIgnoreCase(CodPostal) && codiso.equalsIgnoreCase(CodIso)&& country.equalsIgnoreCase(Country)  ){

                    return true;

                }

            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return false;
    }


    public static boolean validarEmail(String email) {
        Person retorno = null;
        try {

            String query = "Select * from Person";

            Statement st = Connect.connectDb().createStatement();


            ResultSet rs = st.executeQuery(query);


            while (rs.next()) {
              String email1=rs.getString("Email");

           if(email1.equalsIgnoreCase(email)){

               return true;

           }

            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return false;
    }

    /**
     * este método é um regex para validar um email
     * @param email recebe uma string
     * @return retorna true se o email for válido  e False se for inválido
     */
    public static boolean validarEmailRegex(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }
    public static boolean validarPasswordRegex(String password) {
        String regex = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.])(?=\\S+$).{8,24}";
        return password.matches(regex);
    }

    /**
     * Este método verifica se o user já existe na base de dadoas
     * @param user recebe uma string
     * @return retorna true se o user existir na base de dados e False se não existir
     */
    public static boolean validarUsername(String user) {
        Person retorno = null;
        try {

            String query = "Select * from Person";

            Statement st = Connect.connectDb().createStatement();


            ResultSet rs = st.executeQuery(query);


            while (rs.next()) {
                String user1=rs.getString("Username");

                if(user1.equalsIgnoreCase(user)){

                    return true;

                }

            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return false;
    }

}
