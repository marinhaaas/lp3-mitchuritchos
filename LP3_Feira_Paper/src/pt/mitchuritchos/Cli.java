package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Cli {
    static Scanner userInput = new Scanner(System.in);

    /**
     * Este método contém / mostra o menu do programa
     */
    public static void mainMenu() {
        String option;
        do {
            clearCli();
            System.out.println("Bem-Vindo à Feira & Paper");
            System.out.println("-------------------------");
            System.out.println("    1 - Entrar como Administrador");
            System.out.println("    2 - Entrar como Fornecedor");
            System.out.println("    3 - Entrar como Funcionário");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2") && !option.equalsIgnoreCase("3"));

        switch (option) {
            case "1":
                loginAdminMenu();
                break;
            case "2":
                loginSupplierMenu();
                break;
            case "3":
                loginWorkerMenu();
                break;
        }
    }

    /**
     * Este método é o menu de login do Administrador
     */
    private static void loginAdminMenu() {
        String state = null;
        Admin admin;
        do {
            clearCli();
            System.out.print("Insira o seu username de ADMINISTRADOR: ");
            String username = userInput.nextLine();
            System.out.print("Insira a sua password: ");
            String password = userInput.nextLine();
            state = Admin.loginAdmin(username, password);
            admin = Admin.getByUsernameAndPassword(username, password);
        } while (state == null || state.equals(""));
        adminMenu(admin);
    }

    /**
     * Este método é o menu de login do Fornecedor (Supplier)
     */
    public static void loginSupplierMenu() {
        String state = null;
        Supplier supplier;
        do {
            clearCli();
            System.out.print("Insira o seu username de FORNECEDOR: ");
            String username = userInput.nextLine();
            System.out.print("Insira a sua password: ");
            String password = userInput.nextLine();
            state = Supplier.loginSupplier(username, password);
            supplier = Supplier.getByUsernameAndPassword(username, password);
        } while (state == null || state.equals(""));
        supplierMenu(supplier);
    }

    /**
     * Este método é o menu de login do Operador (Worker)
     */
    private static void loginWorkerMenu() {
        String state = null;
        Worker worker;
        do {
            clearCli();
            System.out.print("Insira o seu username de FUNCIONÁRIO: ");
            String username = userInput.nextLine();
            System.out.print("Insira a sua password: ");
            String password = userInput.nextLine();
            state = Worker.loginWorker(username, password);
            worker = Worker.getByUsernameAndPassword(username, password);
        } while (state == null || state.equals(""));
        workerMenu(worker);
    }

    /**
     * Este método mostra o menu do Administrador após o seu Login
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminMenu(Admin admin) {
        String option;
        do {
            clearCli();
            System.out.println("Bem-Vind@ " + admin.getname());
            System.out.println("----------------------------");
            System.out.println("    1 - Criar Utilizadores");
            System.out.println("    2 - Modificar Utilizadores");
            System.out.println("    3 - Apagar Utilizadores");
            System.out.println("    4 - Listar Utilizadores");
            System.out.println("    5 - Verificar Stock");
            System.out.println("    6 - Aprovar Entrada de Stock");
            System.out.println("    7 - Consultar Movimentos");
            System.out.println("    8 - Sair");

            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2") && !option.equalsIgnoreCase("3") && !option.equalsIgnoreCase("4") && !option.equalsIgnoreCase("5") && !option.equalsIgnoreCase("6") && !option.equalsIgnoreCase("7")&& !option.equalsIgnoreCase("8"));

        Worker worker = new Worker("","","","");
        switch (option) {
            case "1":
                adminCreateMenu(admin);
                break;
            case "2":
                adminUpdateMenu(admin);
                break;
            case "3":
                adminDeleteMenu(admin);
                break;
            case "4":
                adminListMenu(admin);
                break;
            case "5":
                Cli.clearCli();
                workerStockVerify(worker);
                backToAdminMenu(admin);
                break;
            case "6":
                Cli.clearCli();
                System.out.println("Stock por aprovar: ");
                System.out.println("-------------------------");
                ArrayList<Stock> list = Stock.getNotApprovedStockList();
                if (list.size() < 1) {
                    System.out.println("");
                    System.out.println("Não existe stock por aprovar...");
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        System.out.println((i + 1) + ") Fornecedor " + list.get(i).getSupplierid() + ": " + list.get(i).getQuantitySheet() + " folhas " + list.get(i).getPaperWidth() + "x" + list.get(i).getPaperLength() + " de " + list.get(i).getPaperGsm() + "gsm.");
                    }
                    System.out.print("Insira o número da linha a aprovar: ");
                    option = userInput.nextLine();
                    list.get(Integer.parseInt(option) - 1).aprovar();
                }
                backToAdminMenu(admin);
                break;
            case "7":
                Cli.clearCli();
                System.out.println("Consulta de Movimentos: ");
                System.out.println("-------------------------");
                Stock.checkMovements();
                backToAdminMenu(admin);
                break;
            case "8":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    /**
     * Este método mostra o menu para registar um Operador (Worker) e um Fornecedor (Supplier)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminCreateMenu(Admin admin) {
        String option;
        do {
            clearCli();
            System.out.println("-------------------------");
            System.out.println("    1 - Criar Operador");
            System.out.println("    2 - Criar Fornecedor");
            System.out.println("    3 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2")&& !option.equalsIgnoreCase("3"));

        switch (option) {
            case "1":
                Cli.clearCli();
                adminCreateWorkerMenu(admin);
                break;
            case "2":
                Cli.clearCli();
                adminCreateSupplierMenu(admin);
                break;
            case "3":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }


    /**
     * Este método mostra o menu para listar os Operadores (Worker) e os Fornecedores (Supplier)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminListMenu(Admin admin) {
        String option;
        do {
            clearCli();
            System.out.println("-------------------------");
            System.out.println("    1 - Listar Operadores");
            System.out.println("    2 - Listar Fornecedores");
            System.out.println("    3 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2")&& !option.equalsIgnoreCase("3"));

        switch (option) {
            case "1":
                Cli.clearCli();
                listarOperadores(admin);
                break;
            case "2":
                Cli.clearCli();
                listarFornecedores(admin);
                break;
            case "3":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    public static void listarOperadores(Admin admin) {
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Worker.IDPerson as Id, Person.Name as name, Person.Username as username, Person.Email as email FROM Worker INNER JOIN Person ON Worker.IDPerson = Person.ID");
            Integer counter = 0;
            System.out.println("Lista de Operários:");
            while (rs.next())
            {
                counter++;
                System.out.println(counter + " ) " + rs.getString("name") + " - " + rs.getString("username") + " - " + rs.getString("email"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }
        backToAdminMenu(admin);
    }
    public static void listarFornecedores(Admin admin) {
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Supplier.City as cidade, Supplier.Country as pais, Supplier.SupplierID as supId, Person.Name as name, Person.Email as email FROM Supplier INNER JOIN Person ON Supplier.IDPerson = Person.ID");

            Integer counter = 0;
            System.out.println("Lista de Fornecedores:");
            while (rs.next())
            {
                counter++;
                System.out.println(counter + " ) " + rs.getString("name") + " - " + rs.getString("email") + " - " + rs.getString("cidade") + " - " + rs.getString("pais") + " - " + rs.getString("supID"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }
        backToAdminMenu(admin);
    }


    /**
     * Este método mostra o menu para apagar o Operador (Worker) e o Fornecedor (Supplier)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminDeleteMenu(Admin admin) {
        String option;
        do {
            clearCli();
            System.out.println("-------------------------");
            System.out.println("    1 - Apagar Operador");
            System.out.println("    2 - Apagar Fornecedor");
            System.out.println("    3 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2")&& !option.equalsIgnoreCase("3"));

        switch (option) {
            case "1":
                adminDeleteWorkerMenu(admin);
                break;
            case "2":
                adminDeleteSupplierMenu(admin);
                break;
            case "3":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    /**
     * Este método mostra um "formulário" para apagar um Operador
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminDeleteWorkerMenu(Admin admin) {
        String option;
        do {
            Cli.clearCli();
            System.out.print("Insira o email do Operador a Apagar:");
            option = userInput.nextLine();
        } while (option.equalsIgnoreCase(""));

        Worker worker = new Worker("","","", option);

        if (worker.delete()) {
            System.out.println("O Fornecedor indicado foi apagado com sucesso!");
        } else {
            System.out.println("Erro a apagar o fornecedor...");
        }

        backToAdminMenu(admin);
    }

    /**
     * Este método mostra um "formulário" para apagar um Fornecedor
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminDeleteSupplierMenu(Admin admin) {
        String option;
        do {
            Cli.clearCli();
            System.out.print("Insira o ID do Fornecedor a Apagar:");
            option = userInput.nextLine();
        } while (option.equalsIgnoreCase(""));

        Supplier supplier = new Supplier("","","","","","","","","","", "", option);

        if (supplier.delete()) {
            System.out.println("O Fornecedor indicado foi apagado com sucesso!");
        } else {
            System.out.println("Erro a apagar o fornecedor...");
        }

        backToAdminMenu(admin);
    }

    /**
     * Este método mostra o menu para modificar o Operador (Worker) e o Fornecedor (Supplier)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminUpdateMenu(Admin admin) {
        String option;
        do {
            clearCli();
            System.out.println("---------------------------");
            System.out.println("    1 - Modificar Operador");
            System.out.println("    2 - Modificar Fornecedor");
            System.out.println("    3 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2")&& !option.equalsIgnoreCase("3"));

        switch (option) {
            case "1":
                do {
                    Cli.clearCli();
                    System.out.print("Insira o email do Operador a Modificar:");
                    option = userInput.nextLine();
                } while (Worker.getByEmail(option) == null);
                adminUpdateWorkerMenu(admin, Worker.getByEmail(option));
                break;
            case "2":
                do {
                    Cli.clearCli();
                    System.out.print("Insira o ID do Fornecedor a Modificar:");
                    option = userInput.nextLine();
                } while (Supplier.getById(option) == null);
                adminUpdateSupplierMenu(admin, Supplier.getById(option));
                break;
            case "3":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    /**
     * Este método mostra o menu para modificar os dados do Operador (Worker)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     * @param worker Recebe como parâmetro o objeto Worker
     */
    private static void adminUpdateWorkerMenu(Admin admin, Worker worker) {
        String option;

        System.out.print("Novo nome do Operador: ");
        String name = userInput.nextLine();
        String username = null;
        do{
            System.out.print("Username do Operador: ");
            username = userInput.nextLine();
            if (username.equalsIgnoreCase(worker.getUsername())) {
                break;
            }
        }while(Validation.validarUsername(username));
        String password = null;
        do{
            System.out.print("Password do Operador: ");
            password = userInput.nextLine();

        }while(password == null || Validation.validarPasswordRegex(password)==false);
        String email = null;
        do{
            System.out.print("Email do Operador: ");
            email = userInput.nextLine();
            if (email.equalsIgnoreCase(worker.getEmail())) {
                break;
            }
        }while(Validation.validarEmail(email) || !Validation.validarEmailRegex(email));

        worker.setname(name);
        worker.setUsername(username);
        worker.setPassword(password);
        worker.setEmail(email);

        if (worker.update()) {
            System.out.println("O Funcionário " + worker.getname() + " foi modificado com sucesso!");
        } else {
            System.out.println("Erro a modificar o funcionário...");
        }

        backToAdminMenu(admin);
    }

    /**
     * Este método mostra o menu para modificar os dados do Fornecedor (Supplier)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     * @param supplier Recebe como parâmetro o objeto Supplier
     */
    private static void adminUpdateSupplierMenu(Admin admin, Supplier supplier) {
        String option, id;
        System.out.print("Novo Nome do Fornecedor: ");
        String name = userInput.nextLine();
        do {
            System.out.print("Insira um ID do Fornecedor válido: ");
            id = userInput.nextLine();
            if (id.equalsIgnoreCase(supplier.getSupplierID())) {
                break;
            }
        } while (Supplier.checkIfIDExists(id));
        String username = null;
        do{
            System.out.print("Novo Username do Fornecedor: ");
            username = userInput.nextLine();
            if (username.equalsIgnoreCase(supplier.getUsername())) {
                break;
            }
        }while(Validation.validarUsername(username));
        String password = null;
        do{
            System.out.print("Password do Operador: ");
            password = userInput.nextLine();

        }while(password == null || Validation.validarPasswordRegex(password)==false);

        String email = null;
        do{
            System.out.print("Novo Email do Fornecedor: ");
            email = userInput.nextLine();
            if (email.equalsIgnoreCase(supplier.getEmail())) {
                break;
            }
        }while(email == null || Validation.validarEmail(email) || !Validation.validarEmailRegex(email));
        System.out.print("Nova Primeira linha de morada: ");
        String morada1 = userInput.nextLine();
        System.out.print("Nova Segunda linha de morada: ");
        String morada2 = userInput.nextLine();
        System.out.print("Nova Cidade: ");
        String cidade = userInput.nextLine();
        System.out.print("Novo Código-Postal: ");
        String cod_postal = userInput.nextLine();
        System.out.print("Novo País: ");
        String pais = userInput.nextLine();
        System.out.print("Novo Código ISO do País: ");
        String codigo_iso = userInput.nextLine();

        supplier.setname(name);
        supplier.setUsername(username);
        supplier.setPassword(password);
        supplier.setEmail(email);
        supplier.setMorada1(morada1);
        supplier.setMorada2(morada2);
        supplier.setCidade(cidade);
        supplier.setCod_postal(cod_postal);
        supplier.setCodigo_iso(codigo_iso);
        supplier.setPais(pais);
        supplier.setSupplierID(id);

        if (supplier.update()) {
            System.out.println("O Fornecedor " + supplier.getname() + " foi modificado com sucesso!");
        } else {
            System.out.println("Erro a modificar o fornecedor...");
        }

        backToAdminMenu(admin);
    }

    /**
     * Este método mostra um menu para registar um Operador (Worker)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminCreateWorkerMenu(Admin admin) {
        String option;

        System.out.print("Nome do Operador: ");
        String name = userInput.nextLine();
        String username = null;
        do{
            System.out.print("Username do Operador: ");
            username = userInput.nextLine();
        }while(username == null || Validation.validarUsername(username));
        String password = null;
        do{
            System.out.print("Password do Operador: ");
            password = userInput.nextLine();

        }while(password == null || Validation.validarPasswordRegex(password)==false);
        String email = null;
        do{
            System.out.print("Email do Operador: ");
            email = userInput.nextLine();
        }while(email == null || Validation.validarEmail(email) || !Validation.validarEmailRegex(email));

        Worker newWorker = new Worker(name, username, password, email);

        if (newWorker.register()) {
            System.out.println("O Funcionário " + newWorker.getname() + " foi registado com sucesso!");
        } else {
            System.out.println("Erro a registar o funcionário...");
        }

        backToAdminMenu(admin);
    }

    /**
     * Este método mostra um menu para registar um Fornecedor (Supplier)
     * @param admin Recebe como parâmetro o objeto Admin com a sessão iniciada
     */
    private static void adminCreateSupplierMenu(Admin admin) {
        String option, id;

        System.out.print("Nome do Fornecedor: ");
        String name = userInput.nextLine();
        do {
            System.out.print("Insira um ID do Fornecedor válido: ");
            id = userInput.nextLine();
        } while (Supplier.checkIfIDExists(id));

        String username = null;
        do{
            System.out.print("Username do Fornecedor: ");
            username = userInput.nextLine();
        }while(username == null || Validation.validarUsername(username));

        String password = null;
        do{
            System.out.print("Password do Fornecedor: ");
            password = userInput.nextLine();

        }while(password == null || Validation.validarPasswordRegex(password)==false);


        String email = null;
        do{
            System.out.print("Email do Fornecedor: ");
            email = userInput.nextLine();
        }while(email == null || Validation.validarEmail(email) || !Validation.validarEmailRegex(email));
        System.out.print("Primeira linha de morada: ");
        String morada1 = userInput.nextLine();
        System.out.print("Segunda linha de morada: ");
        String morada2 = userInput.nextLine();
        System.out.print("Cidade: ");
        String cidade = userInput.nextLine();
        System.out.print("Código-Postal: ");
        String cod_postal = userInput.nextLine();
        System.out.print("País: ");
        String pais = userInput.nextLine();
        System.out.print("Código ISO do País: ");
        String codigo_iso = userInput.nextLine();

        Supplier newSupplier = new Supplier(name, username, password, email, morada1, morada2, cidade, cod_postal, codigo_iso, pais, id);

        if (newSupplier.register()) {
            System.out.println("O Fornecedor " + newSupplier.getname() + " foi registado com sucesso!");
        } else {
            System.out.println("Erro a registar o fornecedor...");
        }

        backToAdminMenu(admin);
    }

    /**
     * Este método mostra o menu do Fornecedor (Supplier)
     * @param supplier Recebe como parâmetro o objeto Supplier com a sessão iniciada
     */
    private static void supplierMenu(Supplier supplier) {
        System.out.println("Bem-Vind@ " + supplier.getname());
        String option;
        do {
            clearCli();
            System.out.println("----------------------------");
            System.out.println("    1 - Upload XML de entrada de stock");
            System.out.println("    2 - Upload JSON de entrada de stock");
            System.out.println("    3 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2") && !option.equalsIgnoreCase("3"));

        switch (option) {
            case "1":
                supplierUploadXml(supplier);
                break;
            case "2":
                supplierUploadJson(supplier);
                break;
            case "3":
                Cli.clearCli();
                mainMenu();
                break;

        }
    }



    /**
     * Este método mostra o menu do Trabalhador (Worker)
     * @param worker Recebe como parâmetro o objeto Worker com a sessão iniciada
     */
    private static void workerMenu(Worker worker) {
        System.out.println("Bem-Vind@ " + worker.getname());
        String option;
        do {
            clearCli();
            System.out.println("----------------------------");
            System.out.println("    1 - Verificar Stock");
            System.out.println("    2 - Aprovar Entrada de Stock");
            System.out.println("    3 - Consultar Movimentos");
            System.out.println("    4 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2") && !option.equalsIgnoreCase("3") && !option.equalsIgnoreCase("4"));

        switch (option) {
            case "1":
                Cli.clearCli();
                workerStockVerify(worker);
                backToWorkerMenu(worker);
                break;
            case "2":
                Cli.clearCli();
                System.out.println("Stock por aprovar: ");
                System.out.println("-------------------------");
                ArrayList<Stock> list = Stock.getNotApprovedStockList();
                if (list.size() < 1) {
                    System.out.println("");
                    System.out.println("Não existe stock por aprovar...");
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        System.out.println((i + 1) + ") Fornecedor " + list.get(i).getSupplierid() + ": " + list.get(i).getQuantitySheet() + " folhas " + list.get(i).getPaperWidth() + "x" + list.get(i).getPaperLength() + " de " + list.get(i).getPaperGsm() + "gsm.");
                    }
                    System.out.print("Insira o número da linha a aprovar: ");
                    option = userInput.nextLine();
                    list.get(Integer.parseInt(option) - 1).aprovar();
                }
                backToWorkerMenu(worker);
                break;
            case "3":
                Cli.clearCli();
                System.out.println("Consulta de Movimentos: ");
                System.out.println("-------------------------");
                Stock.checkMovements();
                backToWorkerMenu(worker);
                break;
            case "4":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    /**
     * Este método mostra o Stock atual
     * @param worker Recebe como parâmetro o objeto Worker
     */
    private static void workerStockVerify(Worker worker) {
        System.out.println("Stock Atual:");
        System.out.println("-------------------------");
        Stock.printApprovedStock();
    }

    /**
     * Este método mostra um formulário para inserir um XML
     * @param supplier Recebe como parâmetro o objeto Supplier
     */
    private static void supplierUploadXml(Supplier supplier) {
        InputXml inputXml = null;

        do {
            Cli.clearCli();
            System.out.print("Insira o caminho de um ficheiro XML válido e não duplicado: ");
            String filePath = userInput.nextLine();
            inputXml = new InputXml(filePath);
        } while (Validation.isValidSupplierIdXml(inputXml.getOrderHeader(), supplier) == false || Validation.isDuplicatedInput(inputXml));
        Cli.clearCli();


        for (int i = 0; i < inputXml.getOrderLine().size(); i++) {
            inputXml.getOrderLine().get(i).save(inputXml.getOrderHeader().getReff());
        }
        System.out.println("XML importado com sucesso!");
        inputXml.getOrderHeader().save();
        inputXml.backup();
        backToSupplierMenu(supplier);

    }

    /**
     * Este método mostra um formulário para inserir um JSON
     * @param supplier Recebe como parâmetro o objeto Supplier
     */
    private static void supplierUploadJson(Supplier supplier) {
        InputJson inputJson = null;

        do {
            Cli.clearCli();
            System.out.print("Insira o caminho de um ficheiro JSON válido e não duplicado: ");
            String filePath = userInput.nextLine();
            inputJson = new InputJson(filePath);
        } while (!Validation.isValidSupplierIdXml(inputJson.getOrderHeader(), supplier) || Validation.isDuplicatedInput(inputJson));
        Cli.clearCli();
        inputJson.backup();
        inputJson.getOrderHeader().save();
        for (int i = 0; i < inputJson.getOrderLine().size(); i++) {
            inputJson.getOrderLine().get(i).save(inputJson.getOrderHeader().getReff());
        }
        System.out.println("JSON importado com sucesso!");
        backToSupplierMenu(supplier);
    }

    /**
     * Este método serve para "limpar a consola", imprimindo linhas em branco
     */
    private static void clearCli() {
        for (int i = 0; i <= 30; i++) {
            System.out.println();
        }
    }

    /**
     * Este método regressa ao menu Admin
     * @param admin Recebe um parâmetro Admin com sessão iniciada
     */
    private static void backToAdminMenu(Admin admin) {
        String option;
        do {
            System.out.println("");
            System.out.println("    1 - Voltar ao menu principal");
            System.out.println("    2 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2"));

        switch (option) {
            case "1":
                adminMenu(admin);
                break;
            case "2":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    /**
     * Este método regressa ao menu Worker
     * @param worker Recebe um parâmetro Worker com sessão iniciada
     */
    private static void backToWorkerMenu(Worker worker) {
        String option;
        do {
            System.out.println("");
            System.out.println("    1 - Voltar ao menu principal");
            System.out.println("    2 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2"));

        switch (option) {
            case "1":
                workerMenu(worker);
                break;
            case "2":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }

    /**
     * Este método regressa ao menu Supplier
     * @param supplier Recebe um parâmetro Supplier com sessão iniciada
     */
    private static void backToSupplierMenu(Supplier supplier) {
        String option;
        do {
            System.out.println("");
            System.out.println("    1 - Voltar ao menu principal");
            System.out.println("    2 - Sair");
            System.out.print("Escolha uma das opções: ");
            option = userInput.nextLine();
        } while (!option.equalsIgnoreCase("1") && !option.equalsIgnoreCase("2"));

        switch (option) {
            case "1":
                supplierMenu(supplier);
                break;
            case "2":
                Cli.clearCli();
                mainMenu();
                break;
        }
    }
}
