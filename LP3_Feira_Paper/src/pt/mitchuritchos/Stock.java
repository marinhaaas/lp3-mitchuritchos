package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Stock {
    private int orderLineId;
    private int supplierid;
    private String paperGsm;
    private String paperLength;
    private String paperWidth;
    private String quantitySheet;

    public Stock(int orderLineId, String paperGsm, String paperLength, String paperWidth, String quantitySheet) {
        this.orderLineId = orderLineId;
        this.paperGsm = paperGsm;
        this.paperLength = paperLength;
        this.paperWidth = paperWidth;
        this.quantitySheet = quantitySheet;
    }

    public Stock(int orderLineId, int supplierid, String paperGsm, String paperLength, String paperWidth, String quantitySheet) {
        this.orderLineId = orderLineId;
        this.supplierid = supplierid;
        this.paperGsm = paperGsm;
        this.paperLength = paperLength;
        this.paperWidth = paperWidth;
        this.quantitySheet = quantitySheet;
    }

    public int getOrderLineId() {
        return orderLineId;
    }

    public void setOrderLineId(int orderLineId) {
        this.orderLineId = orderLineId;
    }

    public int getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(int supplierid) {
        this.supplierid = supplierid;
    }

    public String getPaperGsm() {
        return paperGsm;
    }

    public void setPaperGsm(String paperGsm) {
        this.paperGsm = paperGsm;
    }

    public String getPaperLength() {
        return paperLength;
    }

    public void setPaperLength(String paperLength) {
        this.paperLength = paperLength;
    }

    public String getPaperWidth() {
        return paperWidth;
    }

    public void setPaperWidth(String paperWidth) {
        this.paperWidth = paperWidth;
    }

    public String getQuantitySheet() {
        return quantitySheet;
    }

    public void setQuantitySheet(String quantitySheet) {
        this.quantitySheet = quantitySheet;
    }

    /**
     * Este método serve para obter uma lista de objetos Stock de todas as linhas previamente aprovadas
     * @return Lista de objetos "Stock"
     */
    public static ArrayList<Stock> getApprovedStockList() {
        ArrayList<Stock> list = new ArrayList();
        Stock retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("Select Stock.OrderLineID as OrderLineID, Stock.PaperLength as PaperLength, Stock.PaperWidth as PaperWidth, Stock.PaperGsm as PaperGsm, Stock.QuantitySheet as QuantitySheet, OrderHeader.SupplierID as SupplierID From Stock INNER JOIN OrderLine ON Stock.OrderLineID = OrderLine.OrderLineID INNER JOIN OrderHeader ON OrderLine.OrderReference = OrderHeader.OrderReference ORDER BY OrderHeader.SupplierID ASC");

            while (rs.next())
            {
                retorno = new Stock(Integer.parseInt(rs.getString("OrderLineID")), Integer.parseInt(rs.getString("SupplierID")),  rs.getString("PaperGsm"), rs.getString("PaperLength"), rs.getString("PaperWidth"), rs.getString("QuantitySheet"));
                list.add(retorno);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return list;
    }

    /**
     * Este método serve para obter uma lista de objetos Stock de todas as linhas que ainda não foram aprovadas
     * @return Lista de objetos "Stock"
     */
    public static ArrayList<Stock> getNotApprovedStockList() {
        ArrayList<Stock> list = new ArrayList();
        Stock retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("Select OrderLine.OrderLineID, OrderLine.PaperLength, OrderLine.PaperWidth, OrderLine.PaperGsm, OrderLine.QuantitySheet, OrderHeader.SupplierID From OrderLine INNER JOIN OrderHeader ON OrderLine.OrderReference = OrderHeader.OrderReference Where OrderStatus = 0 ORDER BY OrderHeader.SupplierID ASC");

            while (rs.next())
            {
                retorno = new Stock(Integer.parseInt(rs.getString("OrderLineID")), Integer.parseInt(rs.getString("SupplierID")), rs.getString("PaperGsm"), rs.getString("PaperLength"), rs.getString("PaperWidth"), rs.getString("QuantitySheet"));
                list.add(retorno);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return list;
    }



    /**
     * Este método imprime todas as linhas de stock previamente aprovadas
     */
    public static void printApprovedStock() {
        ArrayList<Stock> list = getApprovedStockList();
        for (int i = 0; i < list.size(); i++) {
            System.out.println((i + 1) + ") Fornecedor " + list.get(i).getSupplierid() + ": " + list.get(i).getQuantitySheet() + " folhas " + list.get(i).getPaperWidth() + "x" + list.get(i).getPaperLength() + " de " + list.get(i).getPaperGsm() + "gsm.");
        }
    }

    /**
     * Este método serve para aprovar uma determinada linha de Stock
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean aprovar() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        if (Validation.checkIfStockExists(this) == null) {
            try {
                String query = "EXEC AproveStock " +
                        "@OrderLineID = " + this.orderLineId +
                        ", @PaperGsm = " + this.paperGsm +
                        ", @PaperLength = " + this.paperLength +
                        ", @PaperWidth = " + this.paperWidth +
                        ", @QuantitySheet = " + this.quantitySheet + ";";
                // create the java statement
                Statement st = conn.createStatement();

                // execute the query, and get a java resultset
                if (st.executeUpdate(query) == 1) {
                    retorno = true;
                }

                st.close();
            } catch (Exception e) {
                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
                return false;
            }

        } else {
            try {
                String query = "EXEC AproveExistingStock " +
                        "@OrderLineID = " + this.orderLineId +
                        ", @QuantitySheet = " + String.valueOf(Integer.parseInt(this.quantitySheet) + Integer.parseInt(Validation.checkIfStockExists(this).getQuantitySheet())) + ";";
                // create the java statement
                Statement st = conn.createStatement();

                // execute the query, and get a java resultset
                if (st.executeUpdate(query) == 1) {
                    retorno = true;
                }

                st.close();
            } catch (Exception e) {
                System.err.println("Got an exception! ");
                System.err.println(e.getMessage());
                return false;
            }
        }
        return retorno;
    }

    /**
     * Este método serve para imprimir todos os movimentos de entrada de Stock de papel na empresa
     */
    public static void checkMovements() {
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("EXEC CheckMovements");

            while (rs.next())
            {
                System.out.println(rs.getString("OrderDate").replace(" 00:00:00.0", "") + ") -" + rs.getString("TotalPrice") + "€ | " + rs.getString("QuantitySheet") + " folhas " + rs.getString("PaperWidth") + "x" + rs.getString("PaperLength") + " de " + rs.getString("PaperGsm") + "gsm");
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }
    }
}
