package pt.mitchuritchos;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;

public class Connect {

    /**
     * Este método serve para criar uma conexão entre a aplicação e a base de dados
     * @return Objeto "Connection" referente à ligação
     */
    public static Connection connectDb() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlserver://193.136.62.208;databaseName=grupo5;user=grupo5;password=Montarlegos.");
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }
        return conn;
    }

    /**
     * Este método serve para cifrar uma password em SHA512
     * @param plainText Password "virgem" a ser decifrada
     * @return Hash da password inserida em SHA512
     */
    public static String textToHash(String plainText) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] messageDigest = md.digest(plainText.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
