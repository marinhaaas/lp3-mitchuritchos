package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Person {
    protected String id;
    protected String name;
    protected String username;
    protected String password;
    protected String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public Person(String id, String name, String username, String password, String email) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Person(String name, String username, String password, String email) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Person(String name, String username, String email) {
        this.name = name;
        this.username = username;
        this.email = email;
    }

    /**
     * Este método serve para criar um objeto "Person" a partir do seu ID
     * @param id ID da pessoa a importar
     * @return Objeto Person
     */
    public static Person getById(String id) {
        Person retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Person WHERE ID = '" + id + "'");

            while (rs.next())
            {
                retorno = new Person(rs.getString("Name"), rs.getString("Username"), rs.getString("Email"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }
}
