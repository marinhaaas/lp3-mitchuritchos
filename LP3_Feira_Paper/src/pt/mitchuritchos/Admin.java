package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class Admin extends Person {
    public Admin(String id, String name, String username, String password, String email) {
        super(id, name, username, password, email);
    }

    public Admin(String name, String username, String password, String email) {
        super(name, username, password, email);
    }

    /**
     * Este método serve para um Administrador fazer login na sua conta com username e password
     * @param username Username do administrador
     * @param password Password do administrador
     * @return ID do administrador logado
     */
    public static String loginAdmin(String username, String password) {
        String valor = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Admin.IDPerson FROM Admin INNER JOIN Person ON Admin.IDPerson = Person.ID WHERE Person.Username = '" + username + "' AND Admin.Hash = '" + Connect.textToHash(password) + "'");

            while (rs.next())
            {
                valor = rs.getString("IDPerson");
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return valor;
    }

    /**
     * Este método serve para retornar um objeto Admin com os dados de um administrador registado na base de dados através do seu username e password
     * @param username Username do administraor
     * @param password Password do Administrador
     * @return Objeto Admin
     */
    public static Admin getByUsernameAndPassword(String username, String password) {
        Admin retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Admin INNER JOIN Person ON Admin.IDPerson = Person.ID WHERE Person.Username = '" + username + "' AND Admin.Hash = '" + Connect.textToHash(password) + "'");

            while (rs.next())
            {
                retorno = new Admin(rs.getString("IDPerson"), rs.getString("Name"), rs.getString("Username"), rs.getString("Hash"), rs.getString("Email"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }
}
