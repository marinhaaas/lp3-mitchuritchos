package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class OrderLine {
    private int orderLineId;
    private String productIdentifierBuyer;
    private String productIdentifierSupplier;
    private String paperLength;
    private String paperWidth;
    private String paperGsm;
    private String quantityKilogram;
    private String quantitySheet;
    private String quantityReam;
    private String unitType;
    private String pricePerUnit;
    private String pricePerUnitCount;
    private String taxType;
    private String taxPercent;
    private String taxLocation;
    private String taxPrice;
    private String totalPrice;
    private String orderStatus;


    public OrderLine(String productIdentifierBuyer, String productIdentifierSupplier, String paperLength, String paperWidth, String paperGsm, String quantityKilogram, String quantitySheet, String quantityReam, String unitType, String pricePerUnit, String pricePerUnitCount, String taxType, String taxPercent, String taxLocation, String taxPrice, String totalPrice, String orderStatus) {
        this.productIdentifierBuyer = productIdentifierBuyer;
        this.productIdentifierSupplier = productIdentifierSupplier;
        this.paperLength = paperLength;
        this.paperWidth = paperWidth;
        this.paperGsm = paperGsm;
        this.quantityKilogram = quantityKilogram;
        this.quantitySheet = quantitySheet;
        this.quantityReam = quantityReam;
        this.unitType = unitType;
        this.pricePerUnit = pricePerUnit;
        this.pricePerUnitCount = pricePerUnitCount;
        this.taxType = taxType;
        this.taxPercent = taxPercent;
        this.taxLocation = taxLocation;
        this.taxPrice = taxPrice;
        this.totalPrice = totalPrice;
        this.orderStatus = orderStatus;
    }

    public String getProductIdentifierBuyer() {
        return productIdentifierBuyer;
    }

    public void setProductIdentifierBuyer(String productIdentifierBuyer) {
        this.productIdentifierBuyer = productIdentifierBuyer;
    }

    public String getProductIdentifierSupplier() {
        return productIdentifierSupplier;
    }

    public void setProductIdentifierSupplier(String productIdentifierSupplier) {
        this.productIdentifierSupplier = productIdentifierSupplier;
    }

    public String getPaperLength() {
        return paperLength;
    }

    public void setPaperLength(String paperLength) {
        this.paperLength = paperLength;
    }

    public String getPaperWidth() {
        return paperWidth;
    }

    public void setPaperWidth(String paperWidth) {
        this.paperWidth = paperWidth;
    }

    public String getPaperGsm() {
        return paperGsm;
    }

    public void setPaperGsm(String paperGsm) {
        this.paperGsm = paperGsm;
    }

    public String getQuantityKilogram() {
        return quantityKilogram;
    }

    public void setQuantityKilogram(String quantityKilogram) {
        this.quantityKilogram = quantityKilogram;
    }

    public String getQuantitySheet() {
        return quantitySheet;
    }

    public void setQuantitySheet(String quantitySheet) {
        this.quantitySheet = quantitySheet;
    }

    public String getQuantityReam() {
        return quantityReam;
    }

    public void setQuantityReam(String quantityReam) {
        this.quantityReam = quantityReam;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(String pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public String getPricePerUnitCount() {
        return pricePerUnitCount;
    }

    public void setPricePerUnitCount(String pricePerUnitCount) {
        this.pricePerUnitCount = pricePerUnitCount;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(String taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getTaxLocation() {
        return taxLocation;
    }

    public void setTaxLocation(String taxLocation) {
        this.taxLocation = taxLocation;
    }

    public String getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(String taxPrice) {
        this.taxPrice = taxPrice;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * este método guarda os dados o OrderLine na Base de Dados
     *
     * @param reff
     * @return este método retorna true se for guardado na Base de Dados com sucesso, e false se não guardar
     */
    public boolean save(String reff) {
        Connection conn = Connect.connectDb();
        boolean retorno = false;


        try {
            String query = "EXEC ImportXMLOrderLine " +
                    "@OrderReference = '" + reff +
                    "', @ProductIdentifierBuyer = " + this.productIdentifierBuyer +
                    ", @ProductIdentifierSupplier = " + this.productIdentifierSupplier +
                    ", @PaperLength = " + this.paperLength +
                    ", @PaperWidth = " + this.paperWidth +
                    ", @PaperGsm = " + this.paperGsm +
                    ", @QuantityKilogram = " + this.quantityKilogram +
                    ", @QuantitySheet= " + this.quantitySheet +
                    ", @QuantityReam = " + this.quantityReam +
                    ", @UnitType = '" + this.unitType +
                    "', @PricePerUnit = " + this.pricePerUnit +
                    ", @PricePerUnitCount = " + this.pricePerUnitCount +
                    ", @TotalPrice = " + this.totalPrice +
                    ", @TaxType = '" + this.taxType +
                    "', @TaxPercent = " + this.taxPercent +
                    ", @TaxPrice = " + this.taxPrice +
                    ", @TaxLocation = '" + this.taxLocation + "';";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
        return retorno;
    }
}
