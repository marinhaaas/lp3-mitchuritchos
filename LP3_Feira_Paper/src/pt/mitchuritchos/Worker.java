package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Worker extends Person {
    public Worker(String id, String name, String username, String password, String email) {
        super(id, name, username, password, email);
    }

    public Worker(String name, String username, String password, String email) {
        super(name, username, password, email);
    }

    /**
     * Este método serve para um Trabalhador fazer login na sua conta com username e password
     * @param username Username do trabalhador
     * @param password Password do trabalhador
     * @return ID do trabalhador logado
     */
    public static String loginWorker(String username, String password) {
        String valor = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Worker.IDPerson FROM Worker INNER JOIN Person ON Worker.IDPerson = Person.ID WHERE Person.Username = '" + username + "' AND Worker.Hash = '" + Connect.textToHash(password) + "'");

            while (rs.next())
            {
                valor = rs.getString("IDPerson");
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return valor;
    }

    /**
     * Este método serve para registar um trabalhador
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean register() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {
            String query = "EXEC CreateWorker " +
                    "@Name = '" + this.name +
                    "', @Username = '" + this.username +
                    "', @Email = '" + this.email +
                    "', @Hash = '" + Connect.textToHash(this.password) + "';";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
        return retorno;
    }

    /**
     * Este método apaga um trabalhador da base de dados
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean delete() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {
            String query = "EXEC DeleteWorker '" + this.email + "'";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
        return retorno;
    }

    /**
     * Este método serve para alterar os dados atuais na base de dados de um trabalhador
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean update() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {

            String query = "EXEC UpdateWorker '" + this.name + "', '" + this.username + "', '" + this.email + "', '" + Connect.textToHash(this.password) + "', '" + this.id + "'";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }

        return retorno;
    }


    /**
     * Este método serve para retornar um objeto Worker com os dados de um trabalhador registado na base de dados através do seu username e password
     * @param username Username do trabalhador
     * @param password Password do trabalhador
     * @return Objeto Worker
     */
    public static Worker getByUsernameAndPassword(String username, String password) {
        Worker retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Worker INNER JOIN Person ON Worker.IDPerson = Person.ID WHERE Person.Username = '" + username + "' AND Worker.Hash = '" + Connect.textToHash(password) + "'");

            while (rs.next())
            {
                retorno = new Worker(rs.getString("IDPerson"), rs.getString("Name"), rs.getString("Username"), rs.getString("Hash"), rs.getString("Email"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }


    /**
     * Este método serve para retornar um objeto Worker com os dados de um trabalhador registado na base de dados através do email
     * @param email Email do trabalhador
     * @return Objeto Worker
     */
    public static Worker getByEmail(String email) {
        Worker retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Worker INNER JOIN Person ON Worker.IDPerson = Person.ID WHERE Person.Email = '" + email + "';");

            while (rs.next())
            {
                retorno = new Worker(rs.getString("IDPerson"), rs.getString("Name"), rs.getString("Username"), rs.getString("Hash"), rs.getString("Email"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }
}
