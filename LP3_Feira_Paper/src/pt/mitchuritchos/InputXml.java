package pt.mitchuritchos;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


public class InputXml {
    private String path;

    public InputXml(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    static Scanner userInput = new Scanner(System.in);

    /**
     * este método lê o Header do ficheiro Xml
     * @return este método retorna o objeto OrderHeader com os dados do ficheiro XMl
     */
    public OrderHeader getOrderHeader(){
        OrderHeader retorno = null;
        try {
            File file = new File(this.path);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("OrderConfirmationHeader");
            for (int itr = 0; itr < nodeList.getLength(); itr++) {
                Node node = nodeList.item(itr);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    try {
                      String reff=eElement.getElementsByTagName("OrderConfirmationReference").item(0).getTextContent();
                      String date=eElement.getElementsByTagName("Year").item(0).getTextContent() + eElement.getElementsByTagName("Month").item(0).getTextContent() + eElement.getElementsByTagName("Day").item(0).getTextContent();
                      String supplierId=eElement.getElementsByTagName("PartyIdentifier").item(0).getTextContent();
                        String nome = eElement.getElementsByTagName("Name").item(0).getTextContent();
                        String address1 = eElement.getElementsByTagName("Address1").item(0).getTextContent();
                        String address2 = eElement.getElementsByTagName("Address2").item(0).getTextContent();
                        String city = eElement.getElementsByTagName("City").item(0).getTextContent();
                        String postalCode = eElement.getElementsByTagName("PostalCode").item(0).getTextContent();
                        String country = eElement.getElementsByTagName("Country").item(0).getTextContent();
                        String iso=eElement.getElementsByTagName("Country").item(0).getAttributes().item(0).getTextContent();
                        retorno = new OrderHeader(reff, date, supplierId);
                    } catch (Exception e) {
                        System.out.println("Ficheiro errado(OrderHeader), prima enter para voltar ao login");
                        String option = userInput.nextLine();
                        Cli.loginSupplierMenu();
                        break;
                    }
                }
            }
        }catch (Exception e) {
            System.out.println("Ficheiro errado(Caminho errado), prima enter para voltar ao login");
            String option = userInput.nextLine();
            Cli.loginSupplierMenu();
        }
    return retorno;
    }

    /**
     * este método le o OrderLine do ficheiro XMl
     * @return retorna uma lista com todos os dados do OrderLine
     */
    public ArrayList<OrderLine> getOrderLine(){
        ArrayList<OrderLine> list = new ArrayList();
        try {
            File file = new File(this.path);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("OrderConfirmationLineItem");
            for (int itr = 0; itr < nodeList.getLength(); itr++) {
                Node node = nodeList.item(itr);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    String quantityKilogram="0", quantitySheet="0", quantityReam="0";
                    try {
                        String buyer=eElement.getElementsByTagName("ProductIdentifier").item(0).getTextContent();
                        String supplier=eElement.getElementsByTagName("ProductIdentifier").item(1).getTextContent();
                        String paperLength=eElement.getElementsByTagName("Value").item(0).getChildNodes().item(0).getNodeValue();
                        String paperWidth=eElement.getElementsByTagName("Value").item(1).getChildNodes().item(0).getNodeValue();
                        String detailValue=eElement.getElementsByTagName("DetailValue").item(0).getTextContent();
                        try {
                            NodeList quantidade = eElement.getElementsByTagName("Quantity");

                            for (int i = 0; i< quantidade.getLength(); i++) {
                                Node quant = quantidade.item(i);
                                if (quant.getNodeType() == quant.ELEMENT_NODE) {
                                    Element elementquant = (Element) quant;
                                    String UOM=elementquant.getElementsByTagName("Value").item(0).getAttributes().item(0).getTextContent();
                                    if(UOM.equals("Ream")){
                                        quantityReam  =elementquant.getElementsByTagName("Value").item(0).getTextContent();
                                    }else if(UOM.equals("Sheet")){
                                        quantitySheet  =elementquant.getElementsByTagName("Value").item(0).getTextContent();
                                    }else if(UOM.equals("Kilogram")){
                                        quantityKilogram  =elementquant.getElementsByTagName("Value").item(0).getTextContent();
                                    }
                                }
                            }
                             } catch (Exception e) {
                                e.getMessage();
                        }
                        try {
                            NodeList informationalQuantity = eElement.getElementsByTagName("InformationalQuantity");

                            for (int i = 0; i< informationalQuantity.getLength(); i++) {
                                Node quant = informationalQuantity.item(i);
                                if (quant.getNodeType() == quant.ELEMENT_NODE) {
                                    Element elementquant = (Element) quant;
                                    String UOM=elementquant.getElementsByTagName("Value").item(0).getAttributes().item(0).getTextContent();
                                    if(UOM.equals("Kilogram")){
                                        quantityKilogram  =elementquant.getElementsByTagName("Value").item(0).getTextContent();
                                    }else if(UOM.equals("Sheet")){
                                        quantitySheet  =elementquant.getElementsByTagName("Value").item(0).getTextContent();
                                    }else if(UOM.equals("Ream")){
                                        quantityReam  =elementquant.getElementsByTagName("Value").item(0).getTextContent();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }

                        String unitType=eElement.getElementsByTagName("Value").item(2).getAttributes().item(0).getTextContent();
                        String pricePerUnit=eElement.getElementsByTagName("CurrencyValue").item(0).getTextContent();
                        String pricePerUnitCount=eElement.getElementsByTagName("Value").item(2).getTextContent();
                        String taxType=eElement.getElementsByTagName("TaxAdjustment").item(0).getAttributes().item(1).getTextContent();
                        String taxPercent=eElement.getElementsByTagName("TaxPercent").item(0).getTextContent();
                        String taxLocation=eElement.getElementsByTagName("TaxLocation").item(0).getTextContent();
                        String taxPrice=eElement.getElementsByTagName("CurrencyValue").item(2).getTextContent();
                        String totalPrice=  eElement.getElementsByTagName("CurrencyValue").item(1).getTextContent();
                        String orderStatus=((Element) node).getAttribute("OrderConfirmationLineItemStatusType");
                        if(quantityReam.equals("0") && !quantitySheet.equals("0")){
                           quantityReam = String.valueOf(Double.parseDouble(quantitySheet)/500);
                        }
                        if(!quantityReam.equals("0") && quantitySheet.equals("0")){
                            quantitySheet = String.valueOf(Double.parseDouble(quantityReam)*500);
                        }
                        if(!quantityReam.equals("0") && !quantitySheet.equals("0")&& quantityKilogram.equals("0")){
                            Double lengght2 =Double.valueOf(paperLength)/1000;
                            Double width2 =Double.valueOf(paperWidth)/1000;
                            Double gramr =Double.valueOf(detailValue);
                            Double numero=Double.valueOf(quantitySheet);
                            quantityKilogram = String.valueOf(lengght2*width2*gramr*numero/1000);
                        }
                        if (!String.valueOf(Double.parseDouble(quantitySheet)/500).equals(String.valueOf(Double.parseDouble(quantityReam)*1))){
                            System.out.println("Erro na quantidade de produto");
                            throw new Exception();

                        }else{
                            OrderLine retorno = new OrderLine(buyer, supplier, paperLength, paperWidth, detailValue, quantityKilogram, quantitySheet, quantityReam, unitType, pricePerUnit, pricePerUnitCount, taxType, taxPercent, taxLocation, taxPrice, totalPrice, orderStatus);
                            list.add(retorno);
                        }

                    } catch (Exception e) {
                        System.out.println("Prima enter para voltar ao login");
                        String option = userInput.nextLine();
                        Cli.loginSupplierMenu();
                    }
                }
            }
        }catch (Exception e) {
            System.out.println("Ficheiro errado(Caminho), prima enter para voltar ao login");
            String option = userInput.nextLine();
            Cli.loginSupplierMenu();
        }
        return  list;
    }

    public void backup() {
        try {
            File directory = new File("backups/xml");
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Path src = Paths.get(this.path);
            Path dest = Paths.get("backups\\xml\\encomenda_" + this.getOrderHeader().getReff() + ".xml");
            Files.deleteIfExists(dest);
            Files.copy(src, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Connection conn = Connect.connectDb();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        try {
            String query = "INSERT INTO Backups (OrderReference, Date, Path) VALUES ('" + this.getOrderHeader().getReff() + "', '" + dtf.format(localDate) + "', '" + "backups\\xml\\encomenda_" + this.getOrderHeader().getReff() + ".xml')";
            Statement st = conn.createStatement();
            st.executeUpdate(query);
            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }

}




