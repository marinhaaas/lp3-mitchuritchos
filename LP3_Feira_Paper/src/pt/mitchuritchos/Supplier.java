package pt.mitchuritchos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Supplier extends Person{

    private String morada1;
    private String morada2;
    private String cidade;
    private String cod_postal;
    private String codigo_iso;
    private String pais;
    private String supplierID;

    /**
     * Este método serve para um Fornecedor fazer login na sua conta com username e password
     * @param username Username do fornecedor
     * @param password Password do fornecedor
     * @return ID do fornecedor logado
     */
    public static String loginSupplier(String username, String password) {
        String valor = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Supplier.IDPerson FROM Supplier INNER JOIN Person ON Supplier.IDPerson = Person.ID WHERE Person.Username = '" + username + "' AND Supplier.Hash = '" + Connect.textToHash(password) + "'");

            while (rs.next())
            {
                valor = rs.getString("IDPerson");
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return valor;
    }

    public String getMorada1() {
        return morada1;
    }

    public void setMorada1(String morada1) {
        this.morada1 = morada1;
    }

    public String getMorada2() {
        return morada2;
    }

    public void setMorada2(String morada2) {
        this.morada2 = morada2;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCod_postal() {
        return cod_postal;
    }

    public void setCod_postal(String cod_postal) {
        this.cod_postal = cod_postal;
    }

    public String getCodigo_iso() {
        return codigo_iso;
    }

    public void setCodigo_iso(String codigo_iso) {
        this.codigo_iso = codigo_iso;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Supplier{" +
                "morada1='" + morada1 + '\'' +
                ", morada2='" + morada2 + '\'' +
                ", cidade='" + cidade + '\'' +
                ", cod_postal='" + cod_postal + '\'' +
                ", codigo_iso='" + codigo_iso + '\'' +
                ", pais='" + pais + '\'' +
                '}';
    }

    public Supplier(String id, String name, String username, String password, String email, String morada1, String morada2, String cidade, String cod_postal, String codigo_iso, String pais, String supplierID) {
        super(id, name, username, password, email);
        this.morada1 = morada1;
        this.morada2 = morada2;
        this.cidade = cidade;
        this.cod_postal = cod_postal;
        this.codigo_iso = codigo_iso;
        this.pais = pais;
        this.supplierID = supplierID;
    }

    public Supplier(String name, String username, String password, String email, String morada1, String morada2, String cidade, String cod_postal, String codigo_iso, String pais, String supplierID) {
        super(name, username, password, email);
        this.morada1 = morada1;
        this.morada2 = morada2;
        this.cidade = cidade;
        this.cod_postal = cod_postal;
        this.codigo_iso = codigo_iso;
        this.pais = pais;
        this.supplierID = supplierID;
    }

    /**
     * Este método serve para registar um fornecedor
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean register() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {
            String query = "EXEC CreateSupplier " +
                    "@Name = '" + this.name +
                    "', @Username = '" + this.username +
                    "', @Email = '" + this.email +
                    "', @Hash = '" + Connect.textToHash(this.password) +
                    "', @Address1 = '" + this.morada1 +
                    "', @Address2 = '" + this.morada2 +
                    "', @City = '" + this.cidade +
                    "', @CodPostal = '" + this.cod_postal +
                    "', @CodIso = '" + this.codigo_iso +
                    "', @Country = '" + this.pais +
                    "', @SupplierID = '" + this.supplierID + "';";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
        return retorno;
    }

    /**
     * Este método apaga um fornecedor da base de dados
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean delete() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {
            String query = "EXEC DeleteSupplier '" + this.id + "'";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
        return retorno;
    }

    /**
     * Este método serve para alterar os dados atuais na base de dados de um fornecedor
     * @return True em caso de sucesso, False em caso de erro
     */
    public boolean update() {
        Connection conn = Connect.connectDb();
        boolean retorno = false;

        try {
            String query = "EXEC UpdateSupplier '" + this.name + "', '" + this.username + "', '" + this.email + "', '" + Connect.textToHash(this.password) + "', '" + this.morada1 + "', '" + this.morada2 + "', '" + this.cidade + "', '" + this.cod_postal + "', '" + this.codigo_iso + "', '" + this.pais + "', '" + this.supplierID + "', '" + this.id + "'";
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            if (st.executeUpdate(query) == 1) {
                retorno = true;
            }

            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }

        return retorno;
    }

    /**
     * Este método serve para retornar um objeto Supplier com os dados de um fornecedor registado na base de dados através do seu username e password
     * @param username Username do fornecedor
     * @param password Password do fornecedor
     * @return Objeto Supplier
     */
    public static Supplier getByUsernameAndPassword(String username, String password) {
        Supplier retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Supplier INNER JOIN Person ON Supplier.IDPerson = Person.ID WHERE Person.Username = '" + username + "' AND Supplier.Hash = '" + Connect.textToHash(password) + "'");

            while (rs.next())
            {
                retorno = new Supplier(rs.getString("IDPerson"), rs.getString("Name"), rs.getString("Username"), rs.getString("Hash"), rs.getString("Email"), rs.getString("Address1"), rs.getString("Address2"), rs.getString("City"), rs.getString("CodPostal"), rs.getString("CodIso"), rs.getString("Country"), rs.getString("SupplierID"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }

    /**
     * Este método serve para retornar um objeto Supplier com os dados de um fornecedor registado na base de dados através do ID
     * @param id ID do fornecedor
     * @return Objeto Supplier
     */
    public static Supplier getById(String id) {
        Supplier retorno = null;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Supplier INNER JOIN Person ON Supplier.IDPerson = Person.ID WHERE Supplier.SupplierID = '" + id + "'");

            while (rs.next())
            {
                retorno = new Supplier(rs.getString("IDPerson"), rs.getString("Name"), rs.getString("Username"), rs.getString("Hash"), rs.getString("Email"), rs.getString("Address1"), rs.getString("Address2"), rs.getString("City"), rs.getString("CodPostal"), rs.getString("CodIso"), rs.getString("Country"), rs.getString("SupplierID"));
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }

        return retorno;
    }

    /**
     * Este método verifica se existe algum fornecedor registado com um determinado ID indicado
     * @param id ID a verificar
     * @return True em caso de existir, False em caso de estar livre
     */
    public static boolean checkIfIDExists(String id) {
        boolean retorno = false;
        try {
            Connection conn = Connect.connectDb();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Supplier WHERE SupplierID = '" + id + "'");

            while (rs.next())
            {
                retorno = true;
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Exceção encontrada! ");
            System.err.println(e.getMessage());
        }
        return retorno;
    }

}
