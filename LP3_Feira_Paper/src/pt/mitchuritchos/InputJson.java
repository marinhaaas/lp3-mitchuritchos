package pt.mitchuritchos;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


public class InputJson {
    private String path;

    public InputJson(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    static Scanner userInput = new Scanner(System.in);

    /**
     * este método le o Header do ficheiro Json
     * @return este método returna um objeto com os dados do  Header do ficheiro JSon
     */
    public OrderHeader getOrderHeader() {
        OrderHeader retorno = null;
        try {
            File file = new File(this.path);
            JsonObject jsonObject = new JsonParser().parse(new FileReader(file)).getAsJsonObject();
            String OrderConfirmationReference = jsonObject.getAsJsonObject("OrderConfirmation").getAsJsonObject("OrderConfirmationHeader").get("OrderConfirmationReference").toString();
            String OrderConfirmationIssuedDate = jsonObject.getAsJsonObject("OrderConfirmation").getAsJsonObject("OrderConfirmationHeader").get("OrderConfirmationIssuedDate").toString();
            String supplierId = jsonObject.getAsJsonObject("OrderConfirmation").getAsJsonObject("OrderConfirmationHeader").getAsJsonObject("SupplierParty").getAsJsonObject("PartyIdentifier").get("Code").toString();
            retorno = new OrderHeader(OrderConfirmationReference.replaceAll("[-\"]", ""), OrderConfirmationIssuedDate.replaceAll("[-\"]", ""), supplierId);
        } catch (Exception e) {
            System.out.println("Ficheiro errado(OrderHeader), prima enter para voltar ao login");
            String option = userInput.nextLine();
            Cli.loginSupplierMenu();
        }
        return retorno;
    }

    /**
     * este método le o OrderLine do ficheiro Json
     * @return este método returna uma lista com os dados OrderLine do ficheiro JSON
     */
    public ArrayList<OrderLine> getOrderLine() {
        ArrayList<OrderLine> list = new ArrayList();


        try {
            File file = new File(this.path);
            JsonObject jsonObject = new JsonParser().parse(new FileReader(file)).getAsJsonObject();
            JsonArray arr = jsonObject.getAsJsonObject("OrderConfirmation").getAsJsonArray("OrderConfirmationLineItem");

            for (int i = 0; i < arr.size(); i++) {
                JsonArray arr1 = arr.get(i).getAsJsonObject().getAsJsonObject("Product").getAsJsonArray("ProductIdentifier");
                String buyer = arr1.get(0).getAsJsonObject().get("Code").toString();
                String supplier = arr1.get(1).getAsJsonObject().get("Code").toString();
                String paperLength= arr.get(i).getAsJsonObject().getAsJsonObject("Product").getAsJsonObject("Paper").getAsJsonObject("Sheet").getAsJsonObject("SheetConversionCharacteristics").getAsJsonObject("SheetSize").getAsJsonObject("Length").getAsJsonObject("Value").get("Value").toString();
                String paperWidth=arr.get(i).getAsJsonObject().getAsJsonObject("Product").getAsJsonObject("Paper").getAsJsonObject("Sheet").getAsJsonObject("SheetConversionCharacteristics").getAsJsonObject("SheetSize").getAsJsonObject("Width").getAsJsonObject("Value").get("Value").toString();
                String detailValue=arr.get(i).getAsJsonObject().getAsJsonObject("Product").getAsJsonObject("Paper").getAsJsonObject("PaperCharacteristics").getAsJsonObject("BasisWeight").getAsJsonObject("DetailValue").get("Value").toString();
                JsonArray arr2 = arr.get(i).getAsJsonObject().getAsJsonArray("InformationalQuantity");
                String quantityKilogram=arr2.get(0).getAsJsonObject().get("Value").getAsJsonObject().get("Value").toString();
                String quantitySheet=arr2.get(1).getAsJsonObject().get("Value").getAsJsonObject().get("Value").toString();
                String quantityReam=arr.get(i).getAsJsonObject().getAsJsonObject("Quantity").getAsJsonObject("Value").get("Value").toString();
                String unitType=arr.get(i).getAsJsonObject().getAsJsonObject("PriceDetails").getAsJsonObject("PricePerUnit").getAsJsonObject("Value").get("UOM").toString().replaceAll("[-\"]", "");
                String pricePerUnit=arr.get(i).getAsJsonObject().getAsJsonObject("PriceDetails").getAsJsonObject("PricePerUnit").getAsJsonObject("CurrencyValue").get("Value").toString();
                String pricePerUnitCount=arr.get(i).getAsJsonObject().getAsJsonObject("PriceDetails").getAsJsonObject("PricePerUnit").getAsJsonObject("Value").get("Value").toString();
                String taxType=arr.get(i).getAsJsonObject().getAsJsonObject("MonetaryAdjustment").getAsJsonObject("TaxAdjustment").get("TaxType").toString().replaceAll("[-\"]", "");
                String taxPercent=arr.get(i).getAsJsonObject().getAsJsonObject("MonetaryAdjustment").getAsJsonObject("TaxAdjustment").get("TaxPercent").toString();
                String taxLocation=arr.get(i).getAsJsonObject().getAsJsonObject("MonetaryAdjustment").getAsJsonObject("TaxAdjustment").get("TaxLocation").toString().replaceAll("[-\"]", "");
                String taxPrice=arr.get(i).getAsJsonObject().getAsJsonObject("MonetaryAdjustment").getAsJsonObject("TaxAdjustment").getAsJsonObject("TaxAmount").getAsJsonObject("CurrencyValue").get("Value").toString();
                String totalPrice=arr.get(i).getAsJsonObject().getAsJsonObject("MonetaryAdjustment").getAsJsonObject("MonetaryAdjustmentStartAmount").getAsJsonObject("CurrencyValue").get("Value").toString();
                String orderStatus="";




                OrderLine retorno = new OrderLine(buyer, supplier, paperLength, paperWidth, detailValue, quantityKilogram, quantitySheet, quantityReam, unitType, pricePerUnit, pricePerUnitCount, taxType, taxPercent, taxLocation, taxPrice, totalPrice, orderStatus);

                    list.add(retorno);



            }


        } catch (Exception e) {
            System.out.println("Ficheiro errado(OrderConfirmationLineItem), prima enter para voltar ao login");
            String option = userInput.nextLine();
            Cli.loginSupplierMenu();
        }


        return list;

    }

    public void backup() {
        try {
            File directory = new File("backups/json");
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Path src = Paths.get(this.path);
            Path dest = Paths.get("backups\\json\\encomenda_" + this.getOrderHeader().getReff() + ".json");
            Files.copy(src, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Connection conn = Connect.connectDb();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();

        try {
            String query = "INSERT INTO Backups (OrderReference, Date, Path) VALUES ('" + this.getOrderHeader().getReff() + "', '" + dtf.format(localDate) + "', '" + "backups\\json\\encomenda_" + this.getOrderHeader().getReff() + ".json')";
            Statement st = conn.createStatement();
            st.executeUpdate(query);
            st.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
}

